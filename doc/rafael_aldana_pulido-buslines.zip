PK
     vR               src/PK
     vR            	   src/main/PK
     vR               src/main/java/PK
     vR               src/main/java/com/PK
     vR               src/main/java/com/aldana/PK
     9wR            "   src/main/java/com/aldana/BusLines/PK
     9wRCa|Ì  Ì  :   src/main/java/com/aldana/BusLines/BusLinesApplication.javapackage com.aldana.BusLines;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;

import java.util.TreeMap;

@SpringBootApplication
public class BusLinesApplication {

    // To be used when producing JSON data for the target API.
    private static TreeMap<String, JSONObject> lines;
    private static TreeMap<String, TreeMap<String, JSONObject>> linesAndStops;
    private static boolean dataGathered;

    private static RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    /**
     * Consumes JSON data from the source API.
     * Processes the JSON data consumed from the source API.
     * Stores the JSON data processed for the target API.
     */
    private static void gatherData() {
        // Get the following data models provided by the source API
        JSONArray jourModel = getJSONArray("jour");
        JSONArray stopModel = getJSONArray("stop");
        JSONArray lineModel = getJSONArray("line");

        lines = new TreeMap<>();

        for (int i = 0; i < lineModel.length(); i++) {
            JSONObject line = lineModel.getJSONObject(i);
            String lineNumber = line.getString("LineNumber");

            lines.put(lineNumber, line);
        }

        linesAndStops = new TreeMap<>();

        for (int i = 0; i < jourModel.length(); i++) {
            JSONObject jour = jourModel.getJSONObject(i);
            String lineNumber = jour.getString("LineNumber");

            for (int j = 0; j < stopModel.length(); j++) {
                JSONObject stop = stopModel.getJSONObject(j);
                String stopPointNumber = stop.getString("StopPointNumber");

                // Connection between stop and line
                if (stopPointNumber.equals(jour.getString("JourneyPatternPointNumber"))) {
                    TreeMap<String, JSONObject> stops = new TreeMap<>();

                    if (linesAndStops.containsKey(lineNumber)) {
                        stops = linesAndStops.get(lineNumber);
                    }

                    stops.put(stopPointNumber, stop);
                    linesAndStops.put(lineNumber, stops);
                }
            }
        }
    }

    /**
     * @param model the desired data model which must be provided by the source API
     * @return the {@link JSONArray} of {@link JSONObject} for the given data model
     */
    private static JSONArray getJSONArray(String model) {
        final String baseUrl = "https://api.sl.se/api2/linedata.json";
        final String key = "b9dd03927b294365a62f99f2a0b74144";
        final String defaultTransportModeCode = "BUS";

        String url = baseUrl + "?key=" + key + "&model=" + model + "&DefaultTransportModeCode=" + defaultTransportModeCode;
        JSONObject root = new JSONObject(getRestTemplate().getForObject(url, String.class));

        return root.getJSONObject("ResponseData").getJSONArray("Result");
    }

    public static void main(String[] args) {
        SpringApplication.run(BusLinesApplication.class, args);
        gatherData();
        dataGathered = true;
    }

    /**
     * @return the map containing all <line number, line data> items
     */
    public static TreeMap<String, JSONObject> getLines() {
        return lines;
    }

    /**
     * @return the map containing all <line number, <stop number, stop data>> items
     */
    public static TreeMap<String, TreeMap<String, JSONObject>> getLinesAndStops() {
        return linesAndStops;
    }

    /**
     * @return whether the data is gathered from the source API yet
     */
    public static boolean isDataGathered() {
        return dataGathered;
    }

}
PK
     9wR            -   src/main/java/com/aldana/BusLines/controller/PK
     9wRhÒZ?  ?  D   src/main/java/com/aldana/BusLines/controller/BusLinesController.javapackage com.aldana.BusLines.controller;

import com.aldana.BusLines.BusLinesApplication;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/api")
public class BusLinesController {

    private static final String PLEASE_WAIT = "The data is being gathered from the source API, please wait...";

    /**
     * Returns the top lines with most stops on their route.
     * The number of stops is added to the line data.
     * Target API call example:
     * http://localhost:8080/api/topLinesWithMostStops?count=10
     *
     * @param lineCount the number of top (10 by default) lines with most stops on their route to return
     * @return the top lines with most stops on their route, an empty list if lineCount is less than one.
     */
    @GetMapping("/topLinesWithMostStops")
    private Object getTopLinesWithMostStops(@RequestParam(defaultValue = "10") int lineCount) {
        if (!BusLinesApplication.isDataGathered()) {
            return PLEASE_WAIT;
        }

        TreeMap<String, TreeMap<String, JSONObject>> linesAndStops = BusLinesApplication.getLinesAndStops();
        ArrayList<Integer> stopCountArrayList = new ArrayList<>();

        for (String lineNumber : linesAndStops.keySet()) {
            stopCountArrayList.add(linesAndStops.get(lineNumber).size());
        }

        Object[] stopCountArray = stopCountArrayList.toArray();
        Arrays.sort(stopCountArray);

        int lineCountThStopCount;

        if (lineCount > stopCountArray.length) {
            lineCountThStopCount = 0;
        } else if (lineCount > 0) {
            lineCountThStopCount = (int) stopCountArray[stopCountArray.length - lineCount];
        } else {
            lineCountThStopCount = Integer.MAX_VALUE;
        }

        ArrayList<String> topLineNumbers = new ArrayList<>();

        for (String lineNumber : linesAndStops.keySet()) {
            if (linesAndStops.get(lineNumber).size() >= lineCountThStopCount) {
                // Lines with the same number of stops as the lineCount-th one should also be added
                topLineNumbers.add(lineNumber);
            }
        }

        JSONArray topLinesWithMostStops = new JSONArray();
        TreeMap<String, JSONObject> lines = BusLinesApplication.getLines();

        if (lines != null) {
            for (String lineNumber : topLineNumbers) {
                JSONObject line = lines.get(lineNumber);

                if (line != null) {
                    // It may be interesting to put the number of stops as well
                    topLinesWithMostStops.put(line.put("StopPointCount", "" + linesAndStops.get(lineNumber).size()));
                }
            }
        }

        return topLinesWithMostStops.toString();
    }

    /**
     * Returns a list of every stop of the bus lineNumber provided.
     * Target API call example:
     * http://localhost:8080/api/stops?lineNumber=157
     *
     * @param lineNumber the line whose list of every stop shall be returned
     * @return the list of every stop of the line provided, an empty list if the line provided does not exist.
     */
    @GetMapping("/stops")
    private Object getStops(@RequestParam String lineNumber) {
        if (!BusLinesApplication.isDataGathered()) {
            return PLEASE_WAIT;
        }

        JSONArray stops = new JSONArray();
        TreeMap<String, TreeMap<String, JSONObject>> linesAndStops = BusLinesApplication.getLinesAndStops();

        if (linesAndStops != null) {
            TreeMap<String, JSONObject> line = linesAndStops.get(lineNumber);

            if (line != null) {
                for (JSONObject stop : line.values()) {
                    stops.put(stop);
                }
            }
        }

        return stops.toString();
    }

}
PK
     vR               src/main/resources/PK
     vR×2      )   src/main/resources/application.properties
PK
     vR            	   src/test/PK
     vR               src/test/java/PK
     vR               src/test/java/com/PK
     vR               src/test/java/com/aldana/PK
     vR            "   src/test/java/com/aldana/BusLines/PK
     vR©âÔqÕ   Õ   ?   src/test/java/com/aldana/BusLines/BuslinesApplicationTests.javapackage com.aldana.BusLines;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class BuslinesApplicationTests {

	@Test
	void contextLoads() {
	}

}
PK
     ¹vR=Ü¢V  V     pom.xml<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.4.4</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>
	<groupId>com.aldana</groupId>
	<artifactId>BusLines</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>BusLines</name>
	<description>Home assignment - Bus lines - API</description>
	<properties>
		<java.version>1.8</java.version>
	</properties>
	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.json</groupId>
			<artifactId>json</artifactId>
			<version>20210307</version>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
		</plugins>
	</build>

</project>
PK 
     vR             $              src/
         g÷~+×g÷~+×[~+×PK 
     vR            	 $          "   src/main/
         {Ð~+×{Ð~+×[~+×PK 
     vR             $          I   src/main/java/
         ~+×~+×[~+×PK 
     vR             $          u   src/main/java/com/
         ~+×~+×~+×PK 
     vR             $          ¥   src/main/java/com/aldana/
         ~+×~+×~+×PK 
     9wR            " $          Ü   src/main/java/com/aldana/BusLines/
         Ú«×T	A«×~+×PK 
     9wRCa|Ì  Ì  : $             src/main/java/com/aldana/BusLines/BusLinesApplication.java
         Ú«×Ú«×~+×PK 
     9wR            - $          @  src/main/java/com/aldana/BusLines/controller/
         È×«×uôu«×~©~+×PK 
     9wRhÒZ?  ?  D $             src/main/java/com/aldana/BusLines/controller/BusLinesController.java
         È×«×È×«×~©~+×PK 
     vR             $          ,!  src/main/resources/
         {Ð~+×{Ð~+×{Ð~+×PK 
     vR×2      ) $           ]!  src/main/resources/application.properties
         {Ð~+×{Ð~+×{Ð~+×PK 
     vR            	 $          ¥!  src/test/
         g÷~+×g÷~+×g÷~+×PK 
     vR             $          Ì!  src/test/java/
         g÷~+×g÷~+×g÷~+×PK 
     vR             $          ø!  src/test/java/com/
         g÷~+×g÷~+×g÷~+×PK 
     vR             $          ("  src/test/java/com/aldana/
         g÷~+×g÷~+×g÷~+×PK 
     vR            " $          _"  src/test/java/com/aldana/BusLines/
         ^~+×^~+×g÷~+×PK 
     vR©âÔqÕ   Õ   ? $           "  src/test/java/com/aldana/BusLines/BuslinesApplicationTests.java
         ^~+×^~+×^~+×PK 
     ¹vR=Ü¢V  V   $           Ñ#  pom.xml
         Ç ãg×Ç ãg×4~+×PK      ½  L)    