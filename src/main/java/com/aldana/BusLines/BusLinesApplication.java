package com.aldana.BusLines;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;

import java.util.TreeMap;

@SpringBootApplication
public class BusLinesApplication {

    // To be used when producing JSON data for the target API.
    private static TreeMap<String, JSONObject> lines;
    private static TreeMap<String, TreeMap<String, JSONObject>> linesAndStops;
    private static boolean dataGathered;

    private static RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    /**
     * Consumes JSON data from the source API.
     * Processes the JSON data consumed from the source API.
     * Stores the JSON data processed for the target API.
     */
    private static void gatherData() {
        // Get the following data models provided by the source API
        JSONArray jourModel = getJSONArray("jour");
        JSONArray stopModel = getJSONArray("stop");
        JSONArray lineModel = getJSONArray("line");

        lines = new TreeMap<>();

        for (int i = 0; i < lineModel.length(); i++) {
            JSONObject line = lineModel.getJSONObject(i);
            String lineNumber = line.getString("LineNumber");

            lines.put(lineNumber, line);
        }

        linesAndStops = new TreeMap<>();

        for (int i = 0; i < jourModel.length(); i++) {
            JSONObject jour = jourModel.getJSONObject(i);
            String lineNumber = jour.getString("LineNumber");

            for (int j = 0; j < stopModel.length(); j++) {
                JSONObject stop = stopModel.getJSONObject(j);
                String stopPointNumber = stop.getString("StopPointNumber");

                // Connection between stop and line
                if (stopPointNumber.equals(jour.getString("JourneyPatternPointNumber"))) {
                    TreeMap<String, JSONObject> stops = new TreeMap<>();

                    if (linesAndStops.containsKey(lineNumber)) {
                        stops = linesAndStops.get(lineNumber);
                    }

                    stops.put(stopPointNumber, stop);
                    linesAndStops.put(lineNumber, stops);
                }
            }
        }
    }

    /**
     * @param model the desired data model which must be provided by the source API
     * @return the {@link JSONArray} of {@link JSONObject} for the given data model
     */
    private static JSONArray getJSONArray(String model) {
        final String baseUrl = "https://api.sl.se/api2/linedata.json";
        final String key = "b9dd03927b294365a62f99f2a0b74144";
        final String defaultTransportModeCode = "BUS";

        String url = baseUrl + "?key=" + key + "&model=" + model + "&DefaultTransportModeCode=" + defaultTransportModeCode;
        JSONObject root = new JSONObject(getRestTemplate().getForObject(url, String.class));

        return root.getJSONObject("ResponseData").getJSONArray("Result");
    }

    public static void main(String[] args) {
        SpringApplication.run(BusLinesApplication.class, args);
        gatherData();
        dataGathered = true;
    }

    /**
     * @return the map containing all <line number, line data> items
     */
    public static TreeMap<String, JSONObject> getLines() {
        return lines;
    }

    /**
     * @return the map containing all <line number, <stop number, stop data>> items
     */
    public static TreeMap<String, TreeMap<String, JSONObject>> getLinesAndStops() {
        return linesAndStops;
    }

    /**
     * @return whether the data is gathered from the source API yet
     */
    public static boolean isDataGathered() {
        return dataGathered;
    }

}
