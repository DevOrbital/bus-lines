package com.aldana.BusLines.controller;

import com.aldana.BusLines.BusLinesApplication;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/api")
public class BusLinesController {

    private static final String PLEASE_WAIT = "The data is being gathered from the source API, please wait...";

    /**
     * Returns the top lines with most stops on their route.
     * The number of stops is added to the line data.
     * Target API call example:
     * http://localhost:8080/api/topLinesWithMostStops?count=10
     *
     * @param lineCount the number of top (10 by default) lines with most stops on their route to return
     * @return the top lines with most stops on their route, an empty list if lineCount is less than one.
     */
    @GetMapping("/topLinesWithMostStops")
    private Object getTopLinesWithMostStops(@RequestParam(defaultValue = "10") int lineCount) {
        if (!BusLinesApplication.isDataGathered()) {
            return PLEASE_WAIT;
        }

        TreeMap<String, TreeMap<String, JSONObject>> linesAndStops = BusLinesApplication.getLinesAndStops();
        ArrayList<Integer> stopCountArrayList = new ArrayList<>();

        for (String lineNumber : linesAndStops.keySet()) {
            stopCountArrayList.add(linesAndStops.get(lineNumber).size());
        }

        Object[] stopCountArray = stopCountArrayList.toArray();
        Arrays.sort(stopCountArray);

        int lineCountThStopCount;

        if (lineCount > stopCountArray.length) {
            lineCountThStopCount = 0;
        } else if (lineCount > 0) {
            lineCountThStopCount = (int) stopCountArray[stopCountArray.length - lineCount];
        } else {
            lineCountThStopCount = Integer.MAX_VALUE;
        }

        ArrayList<String> topLineNumbers = new ArrayList<>();

        for (String lineNumber : linesAndStops.keySet()) {
            if (linesAndStops.get(lineNumber).size() >= lineCountThStopCount) {
                // Lines with the same number of stops as the lineCount-th one should also be added
                topLineNumbers.add(lineNumber);
            }
        }

        JSONArray topLinesWithMostStops = new JSONArray();
        TreeMap<String, JSONObject> lines = BusLinesApplication.getLines();

        if (lines != null) {
            for (String lineNumber : topLineNumbers) {
                JSONObject line = lines.get(lineNumber);

                if (line != null) {
                    // It may be interesting to put the number of stops as well
                    topLinesWithMostStops.put(line.put("StopPointCount", "" + linesAndStops.get(lineNumber).size()));
                }
            }
        }

        return topLinesWithMostStops.toString();
    }

    /**
     * Returns a list of every stop of the bus lineNumber provided.
     * Target API call example:
     * http://localhost:8080/api/stops?lineNumber=157
     *
     * @param lineNumber the line whose list of every stop shall be returned
     * @return the list of every stop of the line provided, an empty list if the line provided does not exist.
     */
    @GetMapping("/stops")
    private Object getStops(@RequestParam String lineNumber) {
        if (!BusLinesApplication.isDataGathered()) {
            return PLEASE_WAIT;
        }

        JSONArray stops = new JSONArray();
        TreeMap<String, TreeMap<String, JSONObject>> linesAndStops = BusLinesApplication.getLinesAndStops();

        if (linesAndStops != null) {
            TreeMap<String, JSONObject> line = linesAndStops.get(lineNumber);

            if (line != null) {
                for (JSONObject stop : line.values()) {
                    stops.put(stop);
                }
            }
        }

        return stops.toString();
    }

}
